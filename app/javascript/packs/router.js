import Vue from 'vue'
import VueRouter from 'vue-router'
import WelcomePage from './views/welcome_page.vue'
import UserRegistrationPage from './views/user_registration_page.vue'
import LoginPage from './views/login_page.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', component: WelcomePage },
  { path: '/user-registration', component: UserRegistrationPage },
  { path: '/login', component: LoginPage }
]

export default new VueRouter({ routes: routes })

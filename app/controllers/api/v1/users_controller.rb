class Api::V1::UsersController < ApplicationController
  skip_before_action :authenticate_user!, only: %i(create)

  def create
    user = User.new(user_params)
    user.save!
    render json: { status: 201, user: user }
  rescue
    render json: { status: 400, messages: user.errors.full_messages }
  end

  def show
    user = User.find(params[:id])
    if user
      render json: user
    else
      render json: 'not found'
    end
  end

  private

  def user_params
    params
      .require(:user)
      .permit(:name, :email, :password, :password_confirmation)
  end
end

require 'rails_helper'

describe Api::V1::UsersController, type: :controller do
  describe 'POST #create' do
    let(:user) { attributes_for(:user) }
    let(:user_params) { { user: user } }

    subject { post :create, params: user_params }

    context 'when params is valid' do
      let(:json) { JSON.parse(subject.body) }

      it { expect { subject }.to change(User, :count).by(1) }
      it 'json format' do
        expect(json['status']).to eq 201
        expect(json['user']['name']).to eq user[:name]
        expect(json['user']['email']).to eq user[:email]
      end
    end
  end
end

class Api::V1::SubscriptionsController < ApplicationController
  skip_before_action :authenticate_user!, only: %i(create)

  def create
    subscription = Subscription.new(subscription_params)
    subscription.save!
    render json: { message: 'subscription create success' }
  rescue
    render json: { message: 'subscription create fault' }
  end

  private

  def subscription_params
    params
      .require(:subscription)
      .permit(:type, :content)
  end
end

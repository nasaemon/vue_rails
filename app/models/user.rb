class User < ApplicationRecord
  has_secure_token
  has_secure_password

  validates :name, presence: true, length: { maximum: 50 }
  validates :email, presence: true, length: { maximum: 255 }, uniqueness: true
  validates :password, presence: true, length: { in: 6..20 }
  validates :token, uniqueness: true
end

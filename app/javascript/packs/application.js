import Vue from 'vue'
import Vuetify from 'vuetify'
import router from './router'
import App from '../app.vue'
import('vuetify/dist/vuetify.min.css')
import('bootstrap/dist/css/bootstrap.css')

Vue.use(Vuetify)


window.addEventListener('load', function() {
  const el = document.body.appendChild(document.createElement('root'))
  new Vue({
    el,
    router,
    render: h => h(App)
  })
})

require 'rails_helper'

describe Api::V1::TokensController, type: :controller do
  describe 'GET #index' do
    let(:user) { create(:user) }
    let(:params) { { password: 'password', email: user.email } }

    context 'when valid email and password' do
      subject { get :index, params: params }
      it { expect(JSON.parse(subject.body)['token']).to eq user.token }
    end
  end
end

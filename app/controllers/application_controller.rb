class ApplicationController < ActionController::Base
  include ActionController::HttpAuthentication::Token::ControllerMethods
  # protect_from_forgery with: :exception

  before_action :authenticate_user!

  def authenticate_user!
    return if authentication_token?
    redirect_to root_path
  end

  private

  def authentication_token?
    authenticate_with_http_token do |token, _|
      user = User.find_by(token: token)
      !!user
    end
  end
end

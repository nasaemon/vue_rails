class Api::V1::TokensController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    user = User.find_by(email: params[:email])
    if user&.authenticate(params[:password])
      render json: { token: user.token }
    else
      render json: { messages: 'invalid email or password' }
    end
  end
end

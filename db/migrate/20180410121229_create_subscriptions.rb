class CreateSubscriptions < ActiveRecord::Migration[5.1]
  def change
    create_table :subscriptions do |t|
      t.string :type, null: false
      t.string :content, null: false

      t.timestamps
    end
  end
end

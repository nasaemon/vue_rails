class Subscription < ApplicationRecord
  validates :type, presence: true
  validates :content, presence: true, length: { maximum: 50 }
end

FactoryBot.define do
  factory :user do
    name 'MyString'
    password 'password'
    sequence :email do |n| "test#{n}@gmail.com" end
  end
end

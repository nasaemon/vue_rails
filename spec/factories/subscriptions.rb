FactoryBot.define do
  factory :subscription do
    type "TitleSubscription"
    content "MyString"
  end
end

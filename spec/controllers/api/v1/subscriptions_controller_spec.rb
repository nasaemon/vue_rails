require 'rails_helper'

describe Api::V1::SubscriptionsController, type: :controller do
  describe 'POST #create' do
    let(:subscription) { attributes_for(:subscription) }
    let(:subscription_params) { { subscription: subscription } }

    subject { post :create, params: subscription_params }

    context 'when params is valid' do
      let(:json) { JSON.parse(subject.body) }

      it { expect { subject }.to change(Subscription, :count).by(1) }
      it 'json format' do
        expect(json['message']).to eq 'subscription create success'
      end
    end
  end
end

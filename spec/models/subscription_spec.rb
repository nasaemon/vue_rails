require 'rails_helper'

RSpec.describe Subscription, type: :model do
  let(:subscription) { build(:subscription) }
  it { expect(subscription).to be_valid }

  describe 'validates' do
    it { is_expected.to validate_presence_of(:type) }
    it { is_expected.to validate_presence_of(:content) }

    it { is_expected.to validate_length_of(:content).is_at_most(50) }
  end
end
